
using UnityEngine;

public class Playermovement : MonoBehaviour
{


    public Rigidbody rb;

    public float forwardforce = 2000f;
    public float sidewaysforce = 500f;
    public float upwardsforce = 200f;
    public bool cubeIsOnTheGround = true;
    public Playermovement movement;

    
    void FixedUpdate()
    {
        
        rb.AddForce(0, 0, forwardforce * Time.deltaTime);

        if (Input.GetKey("d") )
        {
            rb.AddForce(sidewaysforce * Time.deltaTime, 0, 0, ForceMode.VelocityChange);
        }


        if (Input.GetKey("a"))
        {
            rb.AddForce(-sidewaysforce * Time.deltaTime, 0, 0, ForceMode.VelocityChange);
        }

        if (rb.position.y < -1f) 
        {
            FindObjectOfType<Gamemanager>().endgame();
        }

        if (Input.GetKey("space") && cubeIsOnTheGround == true) 
        {
            cubeIsOnTheGround = false;
            rb.AddForce(new Vector3(0,8,0), ForceMode.Impulse);
            Debug.Log("jump");
            
        }
                                    

    }
    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }








    private void OnCollisionEnter(Collision collisionInfo)
    {

        if (collisionInfo.collider.tag == "Ground")
        {
            cubeIsOnTheGround = true;

        }


    }

}





